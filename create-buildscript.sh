#!/bin/bash
cat Title.nw Part1.nw Part2.nw Part3.nw Part4.nw Part5.nw Part6.nw > BuildWithGBPg.nw
notangle -Rbuild-gbp.sh BuildWithGBPg.nw > build-gbp.sh &&
t=`date +%c`
sed --in-place \
--expression='s/This is the end, my friend[[:cntrl:]]*/This is the end, my friend/' \
build-gbp.sh
echo "#generated on $t" >> build-gbp.sh
chmod ugo+x build-gbp.sh

notangle -Rbuild-gbp-maven-plugin.sh BuildWithGBPg.nw > build-gbp-maven-plugin.sh &&
t=`date +%c`
sed --in-place \
--expression='s/This is the end, my friend[[:cntrl:]]*/This is the end, my friend/' \
build-gbp-maven-plugin.sh
echo "#generated on $t" >> build-gbp-maven-plugin.sh
chmod ugo+x build-gbp-maven-plugin.sh

notangle -Rbuild-gbp-webext-plugin.sh BuildWithGBPg.nw > \
build-gbp-webext-plugin.sh &&
t=`date +%c`
sed --in-place \
--expression='s/This is the end, my friend[[:cntrl:]]*/This is the end, my friend/' \
build-gbp-webext-plugin.sh
echo "#generated on $t" >> build-gbp-webext-plugin.sh
chmod ugo+x build-gbp-webext-plugin.sh

notangle -Rcreate-po4a.sh BuildWithGBPg.nw > \
create-po4a.sh &&
t=`date +%c`
sed --in-place \
--expression='s/This is the end, my friend[[:cntrl:]]*/This is the end, my friend/' \
create-po4a.sh
echo "#generated on $t" >> create-po4a.sh
chmod ugo+x create-po4a.sh

# Remove auxillary file BuildWithGBPg.nw
rm BuildWithGBPg.nw


