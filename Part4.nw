% !TEX root = BuildWithGBP.nw
\part{Veröffentlichen}
\chapter{Vorbereitungen zum Hochladen} 

<<PrepareUploading>>=
function PrepareUploading {
    # Called by TaskSelect

    cd ${GitPath}
    
    # Check debian/changelog
    if [ -f debian/changelog ]
    then
        less --LINE-NUMBERS debian/changelog
    else
        whiptail --title "This is the end" \
        --msgbox "No changelog - no upload!" 15 60
        exit
    fi

    if ! whiptail --defaultno \
    --yesno "Is the changelog fit for publishing?" --yes-button "Yes" \
    --no-button "No" 15 60
    then
        AskDist
        echo -e "Notice from PrepareUploading: The branch is "${RecentBranch}"\n \
        The distribution is "${RecentBranchD} >> ${log}
        whiptail --title "Please check! (U)" \
        --msgbox "The branch is ${RecentBranch}" 15 60
        
        if [ "${RecentBranchD}" = "sid" ]
        then
            distName="unstable"
        elif [ "${RecentBranchD}" = "experimental" ]
        then
            distName="experimental"
        else
            distName=$(whiptail --title "Name of the distribution" \
            --inputbox "Please insert the name of the distribution\n \
            specified in the changelog" \
            --cancel-button "Cancel" 15 60 3>&2 2>&1 1>&3)
        fi
        
        if [ -z "${distName}" ]
        then
            distName="unstable"
        fi
        
        echo "Another notice from PrepareUploading:\n \
        The distribution is now "${distName} >> ${log}
        whiptail --title "Please check! (U)" \
        --msgbox "The distribution is ${distName}" 15 60
        # making debian/changelog fit for publishing
        gbp dch --release --verbose --debian-branch=${RecentBranch} \
        --distribution=${distName} #--commit
<<PrepareUploading3>>
@
Nach der Erstellung des Changelogs für das Release wird zur Kontrolle 
automatisch der Standardeditor geöffnet. Dies kann nicht vom Skript 
beeinflusst werden.

<<PrepareUploading3>>=
        git add .
        git commit -a

        whiptail --title "Build again" \
        --msgbox "Now the release will be built another time." 15 60
<<PrepareUploading5>>
@

\begin{figure}[h]
\centering
\includegraphics[width=30em,height=30ex]{Pictures/buildRelease.png}
\caption{Fürs Release bauen}
\end{figure}

<<PrepareUploading5>>=

        # Building revision
        DIST=${RecentBranchD} git-pbuilder update

        if [ ${OptFlag} -ne 1 ]
        then
            ForceOrig

            normalOpts="--git-debian-branch="${RecentBranch}" \
            --git-dist="${RecentBranchD}" --git-verbose --git-tag \
            --git-sign-tags"${pbuilderOpt}
            MoreOptions
        fi

        gbp buildpackage --git-debian-branch=${RecentBranch} \
        --git-dist=${RecentBranchD} --git-verbose --git-tag \
        --git-sign-tags${pbuilderOpt}${moreOpts}
        echo "Package ${SourceName} was built using gbp buildpackage." >> ${log}
    else
        if whiptail --title "Building another time?" \
        --yesno "Should the release be build another time?\n(Without tagging)" \
        --yes-button "Yes" --no-button "No" 15 60
        then
            # Building revision
            DIST=${RecentBranchD} git-pbuilder update

            if [ ${OptFlag} -ne 1 ]
            then
                ForceOrig

                normalOpts="--git-debian-branch="${RecentBranch}" \
                --git-dist="${RecentBranchD}" --git-verbose"${pbuilderOpt}
                MoreOptions
            fi

            gbp buildpackage --git-debian-branch=${RecentBranch} \
            --git-dist=${RecentBranchD} --git-verbose${pbuilderOpt}${moreOpts}
            echo "Package ${SourceName} was built using gbp buildpackage." >> ${log}
        fi
    fi
}

<<SelectUploadTarget>>
@
Im folgenden Abschnitt aus der Aufgabenauswahl werden zunächst die Funktionen aufgerufen, um in die Git-Repositorien zu puschen. 

<<TaskSelect9>>=
    if [ ${rcts} -eq 0 ]
    then
        ##############

        # Pushing git repo
    
        ##############

        Upload2OwnServer
        Upload2Salsa

<<TaskSelect10>>
@

\chapter{Hochladen auf Git-Repositorien}

\section{Hochladen nach salsa.debian.org}
\label{sec:SalsaHochladen}

Vor dem Hochladen nach \textit{salsa.debin.org} überprüft das 
Programm-Skript, ob dort überhaupt schon ein entsprechendes Repositorium
vorhanden ist.

<<Upload2Salsa>>=
function Upload2Salsa {
    # Called by TaskSelect

    # Uploading to Salsa

    if whiptail --title "Upload to salsa.debian.org?" \
    --yesno "Should ${SourceName} be uploaded to Salsa?" \
    --yes-button "Yes" --no-button "No" 15 60
    then
        BrowserName=$(echo ${SalsaName} | sed --expression='s/.git$//g')
        BrowserName="https://salsa.debian.org/"${BrowserName}
        wget --spider --verbose --max-redirect=0 \
        --append-output=${log} ${BrowserName}
        if [ $? -ne 0 ]
        then
             whiptail --title "No project found at salsa.debian.org" \
            --msgbox "Please create ${BrowserName} first" 15 60
        else
<<Upload2Salsa5>>
@
Wenn Patch-Queue-Zweige existieren, können diese vor dem Hochladen nach 
\textit{salsa.debian.org} gelöscht werden.

<<Upload2Salsa5>>=
            if echo $(git branch) | grep --quiet 'patch-queue/'
            then
                if whiptail --title "Patch queue branches found:" \
                --yesno "$(git branch | grep 'patch-queue')\n \
                Delete all?" \
                --yes-button "Yes" --no-button "No" 15 60
                then
                    git branch --delete --force \
                    $(git branch | grep 'patch-queue')
                fi
            fi
            if ! whiptail --title "Last stop before upload!" \
            --yesno "Anything all right?" \
            --yes-button "Yes" --no-button "No" 15 60
            then
                exit
            fi
            git push --set-upstream salsa --all >> ${log}
            git push --set-upstream salsa --tags >> ${log}
            echo ${SourceName}" was uploaded to salsa.debian.org." >> ${log}
        fi
    fi
}

<<GettingFingerprint>>
@

\section{Hochladen auf eigenen Git-Server}
\label{sec:HochladenEigenerGitServer}

Das Hochladen auf einen eigenen Git-Server setzt voraus, dass ein solcher 
eingerichtet wurde (Kapitel~\ref{subsec:EigenerGitServer}, 
Seite~\pageref{subsec:EigenerGitServer}).
Ferner ist zuvor sein Name oder seine IP-Adresse einzugeben 
(Kapitel~\ref{sec:NameEigenerGitServer}, 
Seite~\pageref{sec:NameEigenerGitServer}).

<<Upload2OwnServer>>=
function Upload2OwnServer {
    # Called by TaskSelect

    # Uploading to own git server
    if [ -n "$ServerName" ]
    then
        if whiptail --title "Upload to own git server?" \
        --yesno "Should ${SourceName} be uploaded to your own git server?" \
        --yes-button "Yes" --no-button "No" 15 60
        then
            git push --set-upstream home --all >> ${log}
            git push --set-upstream home --tags >> ${log}

            echo ${SourceName}" was uploaded to your git server." >> ${log}
        fi
    fi
}

<<Upload2Salsa>>
@
\chapter{Paket(e) hochladen}
\label{chap:UploadPackage}
In der Funktion \textit{TaskSelect} (Aufgabenauswahl) werden auch die 
Funktionen zum Hochladen der Pakete aufgerufen.

<<TaskSelect10>>=
        ##############

        # Uploading packages

        ##############

        SelectUploadTarget
        CreateSignature
        UploadUsingDput
        Upload2PeopleDO
        UploadLocal
<<TaskSelect11>>
@
Wenn die Funktionen \textit{CreateNewBranch} 
(Kapitel~\ref{sec:NeuenBranchErstellen}, Seite~\pageref{sec:NeuenBranchErstellen}), 
\textit{SelectBranch} (Kapitel~\ref{sec:AuswahlGitBranch}, 
Seite~\pageref{sec:AuswahlGitBranch}) oder \textit{OwnServer}
(Kapitel~\ref{sec:NameEigenerGitServer}, 
Seite~\pageref{sec:NameEigenerGitServer}) aufgerufen wurden, wird die 
Konfigurationsdatei erneut zur Bearbeitung 
angezeigt (Kapitel~\ref{sec:ConfigFileLaden}, 
Seite~\pageref{sec:ConfigFileLaden} und anschließend die Aufgabenauswahl 
erneut aufgerufen.

<<TaskSelect11>>=
    else
        ConfigFileLEC
        CommonTasks
    fi
}

<<StartTasks>>
@

\section{Auswahl des Zielrepositoriums}
<<SelectUploadTarget>>=
function SelectUploadTarget {
    # Called by TaskSelect

    # Select upload target
    Upl=$(whiptail --title "Uploading?" \
    --radiolist "Should the package be uploaded to ftp-master,\n \
    people.d.o or mentors.debian.net?" 15 60 6\
      "0" "No" off \
      "1" "ftp-master" on \
      "2" "people.d.o" off \
      "3" "Mentors" off \
      "4" "Local repository" off --cancel-button "Exit" 3>&2 2>&1 1>&3)

<<SelectUploadTarget1>>
@
\begin{figure}[h]
\centering
\includegraphics[width=30em,height=30ex]{Pictures/UploadTarget.png}
\caption{Ziel des uploads.}
\end{figure}
<<SelectUploadTarget1>>=
    # The order of the conditions is important!
    # 'Cancel' results an empty variable
    if [ -z ${Upl} ] || [ ${Upl} -eq 0 ]
    then
        exit
    fi

    case "${Upl}" in
     1) Upltext="ftp-master";;
     2) Upltext="people.d.o";;
     3) Upltext="Mentors";;
     4) Upltext="local repository";;
    esac

    cd ${PrjPath}

    # Select package
    SelectChangesFile "Upload" # String will be found in ${1}
    UplPaket=${changesa[$paket]}
        
    Version2=$(echo ${UplPaket} | sed --expression="s/^[a-z\-]*_//" | \
    sed --expression="s/-.*$//")
    SourceName1=$(echo ${UplPaket} | sed --expression="s/_.*//1")
    OrigPaket=${SourceName1}"_"${Version2}".orig"
    
    # Final question before uploading starts
    if ! whiptail --title "Upload to ${Upltext}?" \
    --yesno "You want ${UplPaket} upload to ${Upltext}." \
    --yes-button "Yes" --no-button "No" 15 60
    then
        whiptail --title "Bye" --msgbox "Exit" 15 60
        exit
    fi

    echo "${UplPaket} should be uploaded to ${Upltext}." >> ${log}
}

<<Upload2OwnServer>>
@

\section{Vorbereitung - Signatur erzeugen}

Die Funtion \textit{CreateSignature} erzeugt die erforderlichen Signaturen 
mittels \textit{debsign}. \textit{debsign} signiert ein Debian-.changes- 
und -.dsc-Dateipaar mittels GnuPG.

Im Falle des Fehlschlagens wird eine Wiederholung angeboten.

<<CreateSignature>>=
function CreateSignature {
    # Called by TaskSelect and itself

    # Signature using debsign

    GettingFingerprint

    # Signature
    debsign -k${fipr} ${UplPaket}
    if [ $? -ne 0 ]
    then
        if whiptail --title "Signing failed!" \
        --yesno "Signature failed - Retry?" \
        --yes-button "Yes" --no-button "Exit" 15 60
        then
            CreateSignature
        else
            exit
        fi
    fi

    echo "${UplPaket} was signed" >> ${log}
}

<<Upload2Mentors>>
@
\subsection{Fingerprint nutzen}
\label{subsec:GettingFingerprint}

Zum Signieren der hochzuladenden Pakete wir der 
Fingerprint\index{Fingerprint} des Maintainer-Schlüssels benötigt. 
Dies ist der Fingerprint des Schlüssels, der auch im 
\texttt{Debian-Keyring}\index{Debian-Keyring} hinterlegt ist.

Befindet sich keine entsprechende Datei im Verzeichnis der 
Konfigurationsdateien\index{Konfigurationsdatei}, wird eine entsprechende 
Datei erstellt.

<<GettingFingerprint>>=
function GettingFingerprint {
    # Called by CreateSignature

    finchflag=0

    # getting the fingerprint of the key to sign
    if [ -f ${ConfigPath}/fingerprint ]
    then
        . ${ConfigPath}/fingerprint
    else
        mkdir --parents ${ConfigPath}
        finchflag=1
    fi

    if [ -z "${fipr}" ]
    then
        fipr=$(whiptail --title "Your fingerprint" \
        --inputbox "Please insert fingerprint of your key for signing!" \
        --cancel-button "Cancel" 15 60 3>&2 2>&1 1>&3)
        if [ -z "${fipr}" ]
        then
            echo "Please insert fingerprint of your key for signing!"
            read fipr
        fi
        finchflag=1
    fi

    if ! whiptail --title "Fingerprint" \
    --yesno "Is ${fipr} the right fingerprint of the key for signing?" \
    --yes-button "Yes" --no-button "No" 15 60
    then
        fipr=$(whiptail --title "Key for signing" \
        --inputbox "Real fingerprint of the key for signing:" \
        --cancel-button "Cancel" 15 60 3>&2 2>&1 1>&3)
        if [ -z "${fipr}" ]
        then
            echo "Please insert fingerprint of your key for signing!"
            read fipr
        fi
        finchflag=1
    fi

    if [ $finchflag -eq 1 ]
    then
        if [ -f ${ConfigPath}/fingerprint ]
        then
            mv ${ConfigPath}/fingerprint ${ConfigPath}/fingerprint.backup
            whiptail --title "Fingerprint file" \
            --msgbox "You can find the old fingerprint file as\n \
            ${ConfigPath}/fingerprint.backup" 15 60
        fi
        touch ${ConfigPath}/fingerprint
        echo "#!/usr/bin/bash" >> ${ConfigPath}/fingerprint
        echo "fipr="${fipr} >> ${ConfigPath}/fingerprint
        . ${ConfigPath}/fingerprint
    fi
}

<<CreateSignature>>
@
\section{Hochladen mit dput}
\label{sec:DputHochladen}
<<UploadUsingDput>>=
function UploadUsingDput {
    # Called by TaskSelect

    # Uploading using dput

    cd ${PrjPath}/
    if [ ${Upl} -eq 3 ]
    then
        Upload2Mentors
    elif [ ${Upl} -eq 1 ]
    then
        Upload2FtpMaster
    fi
}

<<UploadFilesSelect>>
@
\section{Nach FTP-Master hochladen}
<<Upload2FtpMaster>>=
function Upload2FtpMaster {
    # Called by UploadUsingDput

    # repeat question
    if whiptail --title "Last exit" \
    --yesno "Should the package be uploaded to ftp-master?" \
    --yes-button "Yes" --no-button "No" 15 60
    then
    
        # Checking whether the .changes file is the right one for the upload target
        sourceFlag=$(echo ${UplPaket} | grep --count '_source.')
        expFlag=$(grep --line-number ') experimental; urgency=' ${GitPath}/debian/changelog \
        | grep '^1:')
        echo -e "${UplPaket}:\n${expFlag}\nsourceFlag: ${sourceFlag}" >> ${log}
        # Strip line to isolate release
        expFlag=$(echo ${expFlag} | sed --expression='s/^.*) //' | \
        sed --expression='s/; .*$//')

        if [ -z "${expFlag}" ]
        then
            if [ ${sourceFlag} -eq 0 ]
            then
                if whiptail --title "Uploading?" \
                --yesno "Do you really want to upload a binary package\n \
                to ftp-master?" --yes-button "Yes" --no-button "No" 15 60
                then
                    Dput2FtpMaster
                else
                    echo "Next try to upload"  >> ${log}
                    SelectUploadTarget
                fi
            else
                Dput2FtpMaster
            fi
        else
            if [ $sourceFlag -ge 1 ]
            then
                if whiptail --title "Uploading?" \
                --yesno "Do you really want to upload a source package\n \
                to experimental?" --yes-button "Yes" --no-button "No" 15 60
                then
                    Dput2FtpMaster
                else
                    echo "Next try to upload"  >> ${log}
                    SelectUploadTarget
                fi
            else
                Dput2FtpMaster
            fi
        fi
    fi
}

<<UploadUsingDput>>
@
In der folgenden Funktion erfolgt das Hochladen nach FTP-Master.
<<Dput2FtpMaster>>=
function Dput2FtpMaster {
    # Called by Upload2FtpMaster

    if whiptail --title "Simulate uploading?" \
    --yesno "Should the upload to ftp-master be simulated?" \
    --yes-button "Yes" --no-button "No" 15 60
    then
        dput --simulate ftp-master ${UplPaket}
        echo
        echo "After reading press return!"
        read x
    fi

    if whiptail --title "Uploading to FTP-Master?" \
    --yesno "Everything fine?\n\nShould the package be uploaded to ftp-master now?" \
    --yes-button "Yes" --no-button "No" 15 60
    then
        dput ftp-master ${UplPaket}
        echo "${UplPaket} was uploaded to ${Upltext}." >> ${log}
    fi
}

<<Upload2FtpMaster>>
@

Dieses Paket landet zunächst auf \url{https://incoming.debian.org/debian-buildd/pool/main/} 
bis es vom Build-Daemon (builddd) gebaut und zum Herunterladen zur 
Verfügung gestellt wird. Verfügung

\subsection{Zurückweisung eines Paketes}

Wenn ein Paket von den FTP-Mastern zurückgewiesen wird, sollte beim anschießenden Hochladen des korrigierten Paketes die Revisionsnummer nicht erhöht werden.

\section{Nach Mentors hochladen}
<<Upload2Mentors>>=
function Upload2Mentors {
    # Called by UploadUsingDput

    if whiptail --title "Simulate uploading?" \
    --yesno "Should the upload to Mentors be simulated?" \
    --yes-button "Yes" --no-button "No" 15 60
    then
        dput --simulate mentors ${UplPaket}
        echo
        echo "After reading press return!"
        read x
    fi

    # repeat question 
    if whiptail --title "Uploading?" \
    --yesno "Should the package be uploaded to Mentors?" \
    --yes-button "Yes" --no-button "No" 15 60
    then
        dput mentors ${UplPaket}
        echo "${UplPaket} was uploade to ${Upltext}." >> ${log}
    fi
}

<<Dput2FtpMaster>>
@

\section{Hochladen nach \textit{people.debian.org}}
<<Upload2PeopleDO>>=
function Upload2PeopleDO {
    # Called by TaskSelect

    if [ $Upl -eq 2 ]
    then
        # For people.d.o you can not use dput
        if whiptail --title "Archive on people.d.o" \
        --yesno "Does the directory public_html/${OrigName} \
        already exsist at people.d.o?\n \
        If not you have to enter the following commands\n \
        in a separate terminal:\n\n \
        ssh <user>@people.debian.org\n \
        mkdir --parents public_html/${OrigName}" \
        --yes-button "Yes" --no-button "No" 15 60
        then
            UploadFilesSelect
<<Upload2PeopleDO3>>
@
<<UploadFilesSelect>>=
function UploadFilesSelect {
    # Called by Upload2PeopleDO UploadLocal
    UplFL=$(cat ${UplPaket} | grep --after-context=10 'Files: *')
    UplFL1=$(echo ${UplFL} | sed --expression='s/Files: //')
    i=1
    while [ $i -lt 6 ]
    do
        c=$(expr ${i} '*' '5')
        UplFL2=${UplFL2}" "$(echo $UplFL1 | cut --delimiter=" " -f${c})
        i=$(expr ${i} '+' '1')
    done
}

<<Upload2PeopleDO>>
@
<<Upload2PeopleDO3>>=
            if whiptail --title "Upload file?" \
            --yesno "Should the following files\n${UplFL2}\n \
            have to be uploaded?" --yes-button "Yes" \
            --no-button "No" 15 60
            then
                if [ -z "${pdoaccount}" ]
                then
                    pdoaccount=$(whiptail --title "Account at people.debian.org" \
                    --inputbox "Please insert the name of your account on\n \
                     people.debian.org" \
                     --cancel-button "Cancel" 15 60 3>&2 2>&1 1>&3)
                    if [ -z "${pdoaccount}" ]
                    then
                        echo "Please insert the name of your account on\n \
                        people.debian.org"
                        read pdoaccount
                    fi
                    changeflag=1
                fi

                if ! whiptail --title "Account name" \
                --yesno "The name of your account on people.debian.org:\n \
                ${pdoaccount}" --yes-button "Yes" --no-button "No" 15 60
                then
                    pdoaccount=$(whiptail --title " Account name" \
                    --inputbox "Name of your account on people.debian.org:" \
                    --cancel-button "Cancel" 15 60 3>&2 2>&1 1>&3)
                    if [ -z "${pdoaccount}" ]
                    then
                        echo "Please insert the name of your account on\n \
                        people.debian.org"
                        read pdoaccount
                    fi
                    changeflag=1
                fi

                if [ $changeflag -eq 1 ]
                then
                    echo 'pdoaccount='${pdoaccount} >> ${ConfigPath}${OrigName}
                    changeflag=0
                fi

                cd ${ProjectPath}/${OrigName}
                if scp -p ${UplFL2} ${pdoaccount}@people.debian.org:/home/${pdoaccount}/public_html/${OrigName}
                then
                    echo "${UplFL2} were uploaded to p.d.o." >> ${log}
                else
                    echo "Something went wrong while uploading to p.d.o." >> ${log}
                    echo "Tried to execute this command:" >> ${log}
                    echo  "scp -p "${UplFL2}" "${pdoaccount}"@people.debian.org:/home/"\
                    ${pdoaccount}"/public_html/"${OrigName} >> ${log}
                fi
                pdoarchivetext="If the archive on people.d.o should be\n \
                used too, you have to enter the following commands:\n \
                ssh <user>@people.debian.org\n \
                cd public_html/${OrigName}\n \
                apt-ftparchive packages . > Packages\n \
                apt-ftparchive sources . > Sources\n \
                cat Packages | gzip -c > Packages.gz\n \
                cat Sources | gzip -c > Sources.gz\n \
                apt-ftparchive release . > Release"

                if whiptail --title "Archive on people.d.o" \
                --yesno "${pdoarchivetext}\n\n \
                Do you like to copy and paste these commands?" \
                --yes-button "Yes" --no-button "No" 15 60
                then
                    echo -e $pdoarchivetext
                    echo -e "\nPlease press any key to continue!"
                    read x
                fi
            fi
        fi
    fi
}

<<UpdateLocalRepo>>
@

\section{Lokales Repositorium}

Es kommt immer wieder vor, dass Pakete gebaut werden müssen, die für das eigentliche Projekt als Abhängigkeiten benötigt werden. 
Um die Zeit für den Gang durch die New-Queue zu überbrücken, können diese auch lokal für das Bauen in der \textit{chroot} bereitgestellt werden.

<<UploadLocal>>=
function UploadLocal {
    # Called by TaskSelect
    if [ $Upl -eq 4 ]
    then
        # Provide for local chroot
        UploadFilesSelect
        if whiptail --title " Files Uploaded?" \
        --yesno "Should the following files\n \
        ${UplFL2}\nhave to be uploaded?" 15 60
        then
            cd ${ProjectPath}/${OrigName}
            sudo cp ${UplFL2} /var/local/repository
            UpdateLocalRepo
        fi
    fi
}

<<ChangeEntry>>
@

Die folgende Funktion gibt es bereits als Shellscript unter \textit{/usr/local/bin/LocalNewRepo}.

<<UpdateLocalRepo>>=
function UpdateLocalRepo {
    # Called by UploadLocal
    cd /var/local/repository

    # Make package archives writable
    # (not only for root)
    sudo chmod o+w Packages
    sudo chmod o+w Sources
    sudo chmod o+w Packages.gz
    sudo chmod o+w Sources.gz
    sudo chmod o+w Release

    # Use apt-ftparchive to update package archives
    sudo apt-ftparchive packages . > Packages &&
    sudo apt-ftparchive sources . > Sources &&
    sudo cat Packages | gzip -c > Packages.gz &&
    sudo cat Sources | gzip -c > Sources.gz &&
    sudo apt-ftparchive release . > Release

    # Reset rights
    sudo chmod o-w Packages
    sudo chmod o-w Sources
    sudo chmod o-w Packages.gz
    sudo chmod o-w Sources.gz
    sudo chmod o-w Release
}

<<UploadLocal>>
@
In die \textit{Chroot-Umgebung}, hier \textit{/var/cache/pbuilder/base.cow}, wird in die Datei \textit{/etc/apt/sources.list} folgendes eingefügt:

\begin{verbatim}
deb [trusted=yes] file:///var/local/repository ./
deb-src [trusted=yes] file:///var/local/repository ./
\end{verbatim}

