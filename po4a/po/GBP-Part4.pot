# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Mechtilde & Michael Stehmann
# This file is distributed under the same license as the BuildWithGBP package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: BuildWithGBP Rolling Release\n"
"POT-Creation-Date: 2021-03-21 13:39+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#.  !TEX root = BuildWithGBP.nw% ===> this file was generated automatically by noweave --- better not edit it
#. type: part{#2}
#: GBP-Part4.tex:4
msgid "Veröffentlichen"
msgstr ""

#. type: chapter{#2}
#: GBP-Part4.tex:4
msgid "Vorbereitungen zum Hochladen"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:8
msgid ""
"\\nwfilename{Part4.nw}\\nwbegincode{1}\\sublabel{NW4N9Oy2-2h4AeG-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2h4AeG-1}}}\\moddef{PrepareUploading~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2h4AeG-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwenddeflinemarkup "
"function PrepareUploading \\{ # Called by TaskSelect"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:10
msgid "cd $\\{GitPath\\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:20
msgid ""
"# Check debian/changelog if [ -f debian/changelog ] then less --LINE-NUMBERS "
"debian/changelog else whiptail --title \"This is the end\" \\\\ --msgbox "
"\"No changelog - no upload!\" 15 60 exit fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:30
msgid ""
"if ! whiptail --defaultno \\\\ --yesno \"Is the changelog fit for "
"publishing?\" --yes-button \"Yes\" \\\\ --no-button \"No\" 15 60 then "
"AskDist echo -e \"Notice from PrepareUploading: The branch is "
"\"$\\{RecentBranch\\}\"\\\\n \\\\ The distribution is \"$\\{RecentBranchD\\} "
">> $\\{log\\} whiptail --title \"Please check! (U)\" \\\\ --msgbox \"The "
"branch is $\\{RecentBranch\\}\" 15 60"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:43
msgid ""
"if [ \"$\\{RecentBranchD\\}\" = \"sid\" ] then distName=\"unstable\" elif [ "
"\"$\\{RecentBranchD\\}\" = \"experimental\" ] then distName=\"experimental\" "
"else distName=$(whiptail --title \"Name of the distribution\" \\\\ "
"--inputbox \"Please insert the name of the distribution\\\\n \\\\ specified "
"in the changelog\" \\\\ --cancel-button \"Cancel\" 15 60 3>&2 2>&1 1>&3)  fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:48
msgid "if [ -z \"$\\{distName\\}\" ] then distName=\"unstable\" fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:61
msgid ""
"echo \"Another notice from PrepareUploading:\\\\n \\\\ The distribution is "
"now \"$\\{distName\\} >> $\\{log\\} whiptail --title \"Please check! (U)\" "
"\\\\ --msgbox \"The distribution is $\\{distName\\}\" 15 60 # making "
"debian/changelog fit for publishing gbp dch --release --verbose "
"--debian-branch=$\\{RecentBranch\\} \\\\ --distribution=$\\{distName\\} "
"#--commit "
"\\LA{}PrepareUploading3~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2fQbcs-1}}\\RA{} "
"\\nwnotused{PrepareUploading}\\nwendcode{}\\nwbegindocs{2}\\nwdocspar Nach "
"der Erstellung des Changelogs für das Release wird zur Kontrolle automatisch "
"der Standardeditor geöffnet. Dies kann nicht vom Skript beeinflusst werden."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:65
msgid ""
"\\nwenddocs{}\\nwbegincode{3}\\sublabel{NW4N9Oy2-2fQbcs-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2fQbcs-1}}}\\moddef{PrepareUploading3~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2fQbcs-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-2h4AeG-1}}\\nwenddeflinemarkup "
"git add .  git commit -a"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:70
msgid ""
"whiptail --title \"Build again\" \\\\ --msgbox \"Now the release will be "
"built another time.\" 15 60 "
"\\LA{}PrepareUploading5~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3vUIOc-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-2h4AeG-1}}\\nwendcode{}\\nwbegindocs{4}\\nwdocspar"
msgstr ""

#. type: caption{#2}
#: GBP-Part4.tex:76
msgid "Fürs Release bauen"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:78
msgid "\\nwenddocs{}\\nwbegincode{5}\\sublabel{NW4N9Oy2-3vUIOc-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3vUIOc-1}}}\\moddef{PrepareUploading5~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3vUIOc-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-2fQbcs-1}}\\nwenddeflinemarkup"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:81
msgid "# Building revision DIST=$\\{RecentBranchD\\} git-pbuilder update"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:85 GBP-Part4.tex:107
msgid "if [ $\\{OptFlag\\} -ne 1 ] then ForceOrig"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:91
msgid ""
"normalOpts=\"--git-debian-branch=\"$\\{RecentBranch\\}\" \\\\ "
"--git-dist=\"$\\{RecentBranchD\\}\" --git-verbose --git-tag \\\\ "
"--git-sign-tags\"$\\{pbuilderOpt\\} MoreOptions fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:103
msgid ""
"gbp buildpackage --git-debian-branch=$\\{RecentBranch\\} \\\\ "
"--git-dist=$\\{RecentBranchD\\} --git-verbose --git-tag \\\\ "
"--git-sign-tags$\\{pbuilderOpt\\}$\\{moreOpts\\} echo \"Package "
"$\\{SourceName\\} was built using gbp buildpackage.\" >> $\\{log\\} else if "
"whiptail --title \"Building another time?\" \\\\ --yesno \"Should the "
"release be build another time?\\\\n(Without tagging)\" \\\\ --yes-button "
"\"Yes\" --no-button \"No\" 15 60 then # Building revision "
"DIST=$\\{RecentBranchD\\} git-pbuilder update"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:112
msgid ""
"normalOpts=\"--git-debian-branch=\"$\\{RecentBranch\\}\" \\\\ "
"--git-dist=\"$\\{RecentBranchD\\}\" --git-verbose\"$\\{pbuilderOpt\\} "
"MoreOptions fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:119
msgid ""
"gbp buildpackage --git-debian-branch=$\\{RecentBranch\\} \\\\ "
"--git-dist=$\\{RecentBranchD\\} "
"--git-verbose$\\{pbuilderOpt\\}$\\{moreOpts\\} echo \"Package "
"$\\{SourceName\\} was built using gbp buildpackage.\" >> $\\{log\\} fi fi "
"\\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:123
msgid ""
"\\LA{}SelectUploadTarget~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-44n7uC-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-2fQbcs-1}}\\nwendcode{}\\nwbegindocs{6}\\nwdocspar Im "
"folgenden Abschnitt aus der Aufgabenauswahl werden zunächst die Funktionen "
"aufgerufen, um in die Git-Repositorien zu puschen."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:128
msgid ""
"\\nwenddocs{}\\nwbegincode{7}\\sublabel{NW4N9Oy2-47oStO-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-47oStO-1}}}\\moddef{TaskSelect9~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-47oStO-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwenddeflinemarkup "
"if [ $\\{rcts\\} -eq 0 ] then ##############"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:130
msgid "# Pushing git repo"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:132 GBP-Part4.tex:242
msgid "##############"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:135
msgid "Upload2OwnServer Upload2Salsa"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:138
msgid ""
"\\LA{}TaskSelect10~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1qX7nC-1}}\\RA{} "
"\\nwnotused{TaskSelect9}\\nwendcode{}\\nwbegindocs{8}\\nwdocspar"
msgstr ""

#. type: chapter{#2}
#: GBP-Part4.tex:140
msgid "Hochladen auf Git-Repositorien"
msgstr ""

#. type: section{#2}
#: GBP-Part4.tex:143
msgid "Hochladen nach salsa.debian.org"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:147
msgid ""
"Vor dem Hochladen nach \\textit{salsa.debin.org} überprüft das "
"Programm-Skript, ob dort überhaupt schon ein entsprechendes Repositorium "
"vorhanden ist."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:151
msgid ""
"\\nwenddocs{}\\nwbegincode{9}\\sublabel{NW4N9Oy2-3M9mGy-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3M9mGy-1}}}\\moddef{Upload2Salsa~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3M9mGy-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-1xqyce-1}}\\nwenddeflinemarkup "
"function Upload2Salsa \\{ # Called by TaskSelect"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:153
msgid "# Uploading to Salsa"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:171
msgid ""
"if whiptail --title \"Upload to salsa.debian.org?\" \\\\ --yesno \"Should "
"$\\{SourceName\\} be uploaded to Salsa?\" \\\\ --yes-button \"Yes\" "
"--no-button \"No\" 15 60 then BrowserName=$(echo $\\{SalsaName\\} | sed "
"--expression='s/.git$//g')  "
"BrowserName=\"https://salsa.debian.org/\"$\\{BrowserName\\} wget --spider "
"--verbose --max-redirect=0 \\\\ --append-output=$\\{log\\} "
"$\\{BrowserName\\} if [ $? -ne 0 ] then whiptail --title \"No project found "
"at salsa.debian.org\" \\\\ --msgbox \"Please create $\\{BrowserName\\} "
"first\" 15 60 else "
"\\LA{}Upload2Salsa5~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-4SzZ52-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-1xqyce-1}}\\nwendcode{}\\nwbegindocs{10}\\nwdocspar "
"Wenn Patch-Queue-Zweige existieren, können diese vor dem Hochladen nach "
"\\textit{salsa.debian.org} gelöscht werden."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:196
msgid ""
"\\nwenddocs{}\\nwbegincode{11}\\sublabel{NW4N9Oy2-4SzZ52-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-4SzZ52-1}}}\\moddef{Upload2Salsa5~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-4SzZ52-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-3M9mGy-1}}\\nwenddeflinemarkup "
"if echo $(git branch) | grep --quiet 'patch-queue/' then if whiptail --title "
"\"Patch queue branches found:\" \\\\ --yesno \"$(git branch | grep "
"'patch-queue')\\\\n \\\\ Delete all?\" \\\\ --yes-button \"Yes\" --no-button "
"\"No\" 15 60 then git branch --delete --force \\\\ $(git branch | grep "
"'patch-queue')  fi fi if ! whiptail --title \"Last stop before upload!\" "
"\\\\ --yesno \"Anything all right?\" \\\\ --yes-button \"Yes\" --no-button "
"\"No\" 15 60 then exit fi git push --set-upstream salsa --all >> $\\{log\\} "
"git push --set-upstream salsa --tags >> $\\{log\\} echo $\\{SourceName\\}\" "
"was uploaded to salsa.debian.org.\" >> $\\{log\\} fi fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:199
msgid ""
"\\LA{}GettingFingerprint~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3stkYg-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-3M9mGy-1}}\\nwendcode{}\\nwbegindocs{12}\\nwdocspar"
msgstr ""

#. type: section{#2}
#: GBP-Part4.tex:202
msgid "Hochladen auf eigenen Git-Server"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:209
msgid ""
"Das Hochladen auf einen eigenen Git-Server setzt voraus, dass ein solcher "
"eingerichtet wurde (Kapitel~\\ref{subsec:EigenerGitServer}, "
"Seite~\\pageref{subsec:EigenerGitServer}).  Ferner ist zuvor sein Name oder "
"seine IP-Adresse einzugeben (Kapitel~\\ref{sec:NameEigenerGitServer}, "
"Seite~\\pageref{sec:NameEigenerGitServer})."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:213
msgid ""
"\\nwenddocs{}\\nwbegincode{13}\\sublabel{NW4N9Oy2-1xqyce-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1xqyce-1}}}\\moddef{Upload2OwnServer~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1xqyce-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-4XRSRb-1}}\\nwenddeflinemarkup "
"function Upload2OwnServer \\{ # Called by TaskSelect"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:223
msgid ""
"# Uploading to own git server if [ -n \"$ServerName\" ] then if whiptail "
"--title \"Upload to own git server?\" \\\\ --yesno \"Should "
"$\\{SourceName\\} be uploaded to your own git server?\" \\\\ --yes-button "
"\"Yes\" --no-button \"No\" 15 60 then git push --set-upstream home --all >> "
"$\\{log\\} git push --set-upstream home --tags >> $\\{log\\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:228
msgid ""
"echo $\\{SourceName\\}\" was uploaded to your git server.\" >> $\\{log\\} fi "
"fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:235
msgid ""
"\\LA{}Upload2Salsa~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3M9mGy-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-4XRSRb-1}}\\nwendcode{}\\nwbegindocs{14}\\nwdocspar "
"\\chapter{Paket(e) hochladen} \\label{chap:UploadPackage} In der Funktion "
"\\textit{TaskSelect} (Aufgabenauswahl) werden auch die Funktionen zum "
"Hochladen der Pakete aufgerufen."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:238
msgid ""
"\\nwenddocs{}\\nwbegincode{15}\\sublabel{NW4N9Oy2-1qX7nC-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1qX7nC-1}}}\\moddef{TaskSelect10~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1qX7nC-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-47oStO-1}}\\nwenddeflinemarkup "
"##############"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:240
msgid "# Uploading packages"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:260
msgid ""
"SelectUploadTarget CreateSignature UploadUsingDput Upload2PeopleDO "
"UploadLocal "
"\\LA{}TaskSelect11~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2fcvCo-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-47oStO-1}}\\nwendcode{}\\nwbegindocs{16}\\nwdocspar "
"Wenn die Funktionen \\textit{CreateNewBranch} "
"(Kapitel~\\ref{sec:NeuenBranchErstellen}, "
"Seite~\\pageref{sec:NeuenBranchErstellen}), \\textit{SelectBranch} "
"(Kapitel~\\ref{sec:AuswahlGitBranch}, Seite~\\pageref{sec:AuswahlGitBranch}) "
"oder \\textit{OwnServer} (Kapitel~\\ref{sec:NameEigenerGitServer}, "
"Seite~\\pageref{sec:NameEigenerGitServer}) aufgerufen wurden, wird die "
"Konfigurationsdatei erneut zur Bearbeitung angezeigt "
"(Kapitel~\\ref{sec:ConfigFileLaden}, Seite~\\pageref{sec:ConfigFileLaden} "
"und anschließend die Aufgabenauswahl erneut aufgerufen."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:267
msgid ""
"\\nwenddocs{}\\nwbegincode{17}\\sublabel{NW4N9Oy2-2fcvCo-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2fcvCo-1}}}\\moddef{TaskSelect11~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2fcvCo-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-1qX7nC-1}}\\nwenddeflinemarkup "
"else ConfigFileLEC CommonTasks fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:270
msgid ""
"\\LA{}StartTasks~{\\nwtagstyle{}\\subpageref{nw@notdef}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-1qX7nC-1}}\\nwendcode{}\\nwbegindocs{18}\\nwdocspar"
msgstr ""

#. type: section{#2}
#: GBP-Part4.tex:275
msgid "Auswahl des Zielrepositoriums"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:275
msgid ""
"\\nwenddocs{}\\nwbegincode{19}\\sublabel{NW4N9Oy2-44n7uC-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-44n7uC-1}}}\\moddef{SelectUploadTarget~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-44n7uC-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-3vUIOc-1}}\\nwenddeflinemarkup "
"function SelectUploadTarget \\{ # Called by TaskSelect"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:285
msgid ""
"# Select upload target Upl=$(whiptail --title \"Uploading?\" \\\\ "
"--radiolist \"Should the package be uploaded to ftp-master,\\\\n \\\\ "
"people.d.o or mentors.debian.net?\" 15 60 6\\\\ \"0\" \"No\" off \\\\ \"1\" "
"\"ftp-master\" on \\\\ \"2\" \"people.d.o\" off \\\\ \"3\" \"Mentors\" off "
"\\\\ \"4\" \"Local repository\" off --cancel-button \"Exit\" 3>&2 2>&1 1>&3)"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:300
msgid ""
"\\LA{}SelectUploadTarget1~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-4XRSRb-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-3vUIOc-1}}\\nwendcode{}\\nwbegindocs{20}\\nwdocspar"
msgstr ""

#. type: caption{#2}
#: GBP-Part4.tex:300
msgid "Ziel des uploads."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:300
msgid ""
"\\nwenddocs{}\\nwbegincode{21}\\sublabel{NW4N9Oy2-4XRSRb-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-4XRSRb-1}}}\\moddef{SelectUploadTarget1~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-4XRSRb-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-44n7uC-1}}\\nwenddeflinemarkup "
"# The order of the conditions is important! # 'Cancel' results an empty "
"variable if [ -z $\\{Upl\\} ] || [ $\\{Upl\\} -eq 0 ] then exit fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:307
msgid ""
"case \"$\\{Upl\\}\" in 1) Upltext=\"ftp-master\";; 2) "
"Upltext=\"people.d.o\";; 3) Upltext=\"Mentors\";; 4) Upltext=\"local "
"repository\";; esac"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:309
msgid "cd $\\{PrjPath\\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:313
msgid ""
"# Select package SelectChangesFile \"Upload\" # String will be found in "
"$\\{1\\} UplPaket=$\\{changesa[$paket]\\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:318
msgid ""
"Version2=$(echo $\\{UplPaket\\} | sed --expression=\"s/^[a-z\\\\-]*_//\" | "
"\\\\ sed --expression=\"s/-.*$//\")  SourceName1=$(echo $\\{UplPaket\\} | "
"sed --expression=\"s/_.*//1\")  "
"OrigPaket=$\\{SourceName1\\}\"_\"$\\{Version2\\}\".orig\""
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:327
msgid ""
"# Final question before uploading starts if ! whiptail --title \"Upload to "
"$\\{Upltext\\}?\" \\\\ --yesno \"You want $\\{UplPaket\\} upload to "
"$\\{Upltext\\}.\" \\\\ --yes-button \"Yes\" --no-button \"No\" 15 60 then "
"whiptail --title \"Bye\" --msgbox \"Exit\" 15 60 exit fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:330
msgid ""
"echo \"$\\{UplPaket\\} should be uploaded to $\\{Upltext\\}.\" >> $\\{log\\} "
"\\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:333
msgid ""
"\\LA{}Upload2OwnServer~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1xqyce-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-44n7uC-1}}\\nwendcode{}\\nwbegindocs{22}\\nwdocspar"
msgstr ""

#. type: section{#2}
#: GBP-Part4.tex:335
msgid "Vorbereitung - Signatur erzeugen"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:339
msgid ""
"Die Funtion \\textit{CreateSignature} erzeugt die erforderlichen Signaturen "
"mittels \\textit{debsign}. \\textit{debsign} signiert ein Debian-.changes- "
"und -.dsc-Dateipaar mittels GnuPG."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:341
msgid "Im Falle des Fehlschlagens wird eine Wiederholung angeboten."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:345
msgid ""
"\\nwenddocs{}\\nwbegincode{23}\\sublabel{NW4N9Oy2-fT69x-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-fT69x-1}}}\\moddef{CreateSignature~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-fT69x-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-3stkYg-1}}\\nwenddeflinemarkup "
"function CreateSignature \\{ # Called by TaskSelect and itself"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:347
msgid "# Signature using debsign"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:349
msgid "GettingFingerprint"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:363
msgid ""
"# Signature debsign -k$\\{fipr\\} $\\{UplPaket\\} if [ $? -ne 0 ] then if "
"whiptail --title \"Signing failed!\" \\\\ --yesno \"Signature failed - "
"Retry?\" \\\\ --yes-button \"Yes\" --no-button \"Exit\" 15 60 then "
"CreateSignature else exit fi fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:366
msgid "echo \"$\\{UplPaket\\} was signed\" >> $\\{log\\} \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:371
msgid ""
"\\LA{}Upload2Mentors~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3qFEIZ-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-3stkYg-1}}\\nwendcode{}\\nwbegindocs{24}\\nwdocspar"
msgstr ""

#. type: subsection{#2}
#: GBP-Part4.tex:371
msgid "Fingerprint nutzen"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:376
msgid ""
"Zum Signieren der hochzuladenden Pakete wir der "
"Fingerprint\\index{Fingerprint} des Maintainer-Schlüssels benötigt.  Dies "
"ist der Fingerprint des Schlüssels, der auch im "
"\\texttt{Debian-Keyring}\\index{Debian-Keyring} hinterlegt ist."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:380
msgid ""
"Befindet sich keine entsprechende Datei im Verzeichnis der "
"Konfigurationsdateien\\index{Konfigurationsdatei}, wird eine entsprechende "
"Datei erstellt."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:384
msgid ""
"\\nwenddocs{}\\nwbegincode{25}\\sublabel{NW4N9Oy2-3stkYg-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3stkYg-1}}}\\moddef{GettingFingerprint~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3stkYg-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-4SzZ52-1}}\\nwenddeflinemarkup "
"function GettingFingerprint \\{ # Called by CreateSignature"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:386
msgid "finchflag=0"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:395
msgid ""
"# getting the fingerprint of the key to sign if [ -f "
"$\\{ConfigPath\\}/fingerprint ] then . $\\{ConfigPath\\}/fingerprint else "
"mkdir --parents $\\{ConfigPath\\} finchflag=1 fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:408
msgid ""
"if [ -z \"$\\{fipr\\}\" ] then fipr=$(whiptail --title \"Your fingerprint\" "
"\\\\ --inputbox \"Please insert fingerprint of your key for signing!\" \\\\ "
"--cancel-button \"Cancel\" 15 60 3>&2 2>&1 1>&3)  if [ -z \"$\\{fipr\\}\" ] "
"then echo \"Please insert fingerprint of your key for signing!\" read fipr "
"fi finchflag=1 fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:423
msgid ""
"if ! whiptail --title \"Fingerprint\" \\\\ --yesno \"Is $\\{fipr\\} the "
"right fingerprint of the key for signing?\" \\\\ --yes-button \"Yes\" "
"--no-button \"No\" 15 60 then fipr=$(whiptail --title \"Key for signing\" "
"\\\\ --inputbox \"Real fingerprint of the key for signing:\" \\\\ "
"--cancel-button \"Cancel\" 15 60 3>&2 2>&1 1>&3)  if [ -z \"$\\{fipr\\}\" ] "
"then echo \"Please insert fingerprint of your key for signing!\" read fipr "
"fi finchflag=1 fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:439
msgid ""
"if [ $finchflag -eq 1 ] then if [ -f $\\{ConfigPath\\}/fingerprint ] then mv "
"$\\{ConfigPath\\}/fingerprint $\\{ConfigPath\\}/fingerprint.backup whiptail "
"--title \"Fingerprint file\" \\\\ --msgbox \"You can find the old "
"fingerprint file as\\\\n \\\\ $\\{ConfigPath\\}/fingerprint.backup\" 15 60 "
"fi touch $\\{ConfigPath\\}/fingerprint echo \"#!/usr/bin/bash\" >> "
"$\\{ConfigPath\\}/fingerprint echo \"fipr=\"$\\{fipr\\} >> "
"$\\{ConfigPath\\}/fingerprint . $\\{ConfigPath\\}/fingerprint fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:447
msgid ""
"\\LA{}CreateSignature~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-fT69x-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-4SzZ52-1}}\\nwendcode{}\\nwbegindocs{26}\\nwdocspar "
"\\section{Hochladen mit dput} \\label{sec:DputHochladen} "
"\\nwenddocs{}\\nwbegincode{27}\\sublabel{NW4N9Oy2-38Lz4Y-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-38Lz4Y-1}}}\\moddef{UploadUsingDput~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-38Lz4Y-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-2XJq8-1}}\\nwenddeflinemarkup "
"function UploadUsingDput \\{ # Called by TaskSelect"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:449
msgid "# Uploading using dput"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:459
msgid ""
"cd $\\{PrjPath\\}/ if [ $\\{Upl\\} -eq 3 ] then Upload2Mentors elif [ "
"$\\{Upl\\} -eq 1 ] then Upload2FtpMaster fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:466
msgid ""
"\\LA{}UploadFilesSelect~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1Zg9xf-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-2XJq8-1}}\\nwendcode{}\\nwbegindocs{28}\\nwdocspar "
"\\section{Nach FTP-Master hochladen} "
"\\nwenddocs{}\\nwbegincode{29}\\sublabel{NW4N9Oy2-2XJq8-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2XJq8-1}}}\\moddef{Upload2FtpMaster~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2XJq8-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-2eHzlG-1}}\\nwenddeflinemarkup "
"function Upload2FtpMaster \\{ # Called by UploadUsingDput"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:472
msgid ""
"# repeat question if whiptail --title \"Last exit\" \\\\ --yesno \"Should "
"the package be uploaded to ftp-master?\" \\\\ --yes-button \"Yes\" "
"--no-button \"No\" 15 60 then"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:481
msgid ""
"# Checking whether the .changes file is the right one for the upload target "
"sourceFlag=$(echo $\\{UplPaket\\} | grep --count '_source.')  expFlag=$(grep "
"--line-number ') experimental; urgency=' $\\{GitPath\\}/debian/changelog "
"\\\\ | grep '^1:')  echo -e "
"\"$\\{UplPaket\\}:\\\\n$\\{expFlag\\}\\\\nsourceFlag: $\\{sourceFlag\\}\" >> "
"$\\{log\\} # Strip line to isolate release expFlag=$(echo $\\{expFlag\\} | "
"sed --expression='s/^.*) //' | \\\\ sed --expression='s/; .*$//')"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:516
msgid ""
"if [ -z \"$\\{expFlag\\}\" ] then if [ $\\{sourceFlag\\} -eq 0 ] then if "
"whiptail --title \"Uploading?\" \\\\ --yesno \"Do you really want to upload "
"a binary package\\\\n \\\\ to ftp-master?\" --yes-button \"Yes\" --no-button "
"\"No\" 15 60 then Dput2FtpMaster else echo \"Next try to upload\" >> "
"$\\{log\\} SelectUploadTarget fi else Dput2FtpMaster fi else if [ "
"$sourceFlag -ge 1 ] then if whiptail --title \"Uploading?\" \\\\ --yesno "
"\"Do you really want to upload a source package\\\\n \\\\ to experimental?\" "
"--yes-button \"Yes\" --no-button \"No\" 15 60 then Dput2FtpMaster else echo "
"\"Next try to upload\" >> $\\{log\\} SelectUploadTarget fi else "
"Dput2FtpMaster fi fi fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:523
msgid ""
"\\LA{}UploadUsingDput~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-38Lz4Y-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-2eHzlG-1}}\\nwendcode{}\\nwbegindocs{30}\\nwdocspar "
"In der folgenden Funktion erfolgt das Hochladen nach FTP-Master.  "
"\\nwenddocs{}\\nwbegincode{31}\\sublabel{NW4N9Oy2-2eHzlG-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2eHzlG-1}}}\\moddef{Dput2FtpMaster~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2eHzlG-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-3qFEIZ-1}}\\nwenddeflinemarkup "
"function Dput2FtpMaster \\{ # Called by Upload2FtpMaster"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:533
msgid ""
"if whiptail --title \"Simulate uploading?\" \\\\ --yesno \"Should the upload "
"to ftp-master be simulated?\" \\\\ --yes-button \"Yes\" --no-button \"No\" "
"15 60 then dput --simulate ftp-master $\\{UplPaket\\} echo echo \"After "
"reading press return!\" read x fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:542
msgid ""
"if whiptail --title \"Uploading to FTP-Master?\" \\\\ --yesno \"Everything "
"fine?\\\\n\\\\nShould the package be uploaded to ftp-master now?\" \\\\ "
"--yes-button \"Yes\" --no-button \"No\" 15 60 then dput ftp-master "
"$\\{UplPaket\\} echo \"$\\{UplPaket\\} was uploaded to $\\{Upltext\\}.\" >> "
"$\\{log\\} fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:545
msgid ""
"\\LA{}Upload2FtpMaster~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2XJq8-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-3qFEIZ-1}}\\nwendcode{}\\nwbegindocs{32}\\nwdocspar"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:549
msgid ""
"Dieses Paket landet zunächst auf "
"\\url{https://incoming.debian.org/debian-buildd/pool/main/} bis es vom "
"Build-Daemon (builddd) gebaut und zum Herunterladen zur Verfügung gestellt "
"wird. Verfügung"
msgstr ""

#. type: subsection{#2}
#: GBP-Part4.tex:551
msgid "Zurückweisung eines Paketes"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:553
msgid ""
"Wenn ein Paket von den FTP-Mastern zurückgewiesen wird, sollte beim "
"anschießenden Hochladen des korrigierten Paketes die Revisionsnummer nicht "
"erhöht werden."
msgstr ""

#. type: section{#2}
#: GBP-Part4.tex:558
msgid "Nach Mentors hochladen"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:558
msgid ""
"\\nwenddocs{}\\nwbegincode{33}\\sublabel{NW4N9Oy2-3qFEIZ-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3qFEIZ-1}}}\\moddef{Upload2Mentors~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3qFEIZ-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-fT69x-1}}\\nwenddeflinemarkup "
"function Upload2Mentors \\{ # Called by UploadUsingDput"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:568
msgid ""
"if whiptail --title \"Simulate uploading?\" \\\\ --yesno \"Should the upload "
"to Mentors be simulated?\" \\\\ --yes-button \"Yes\" --no-button \"No\" 15 "
"60 then dput --simulate mentors $\\{UplPaket\\} echo echo \"After reading "
"press return!\" read x fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:578
msgid ""
"# repeat question if whiptail --title \"Uploading?\" \\\\ --yesno \"Should "
"the package be uploaded to Mentors?\" \\\\ --yes-button \"Yes\" --no-button "
"\"No\" 15 60 then dput mentors $\\{UplPaket\\} echo \"$\\{UplPaket\\} was "
"uploade to $\\{Upltext\\}.\" >> $\\{log\\} fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:581
msgid ""
"\\LA{}Dput2FtpMaster~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-2eHzlG-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-fT69x-1}}\\nwendcode{}\\nwbegindocs{34}\\nwdocspar"
msgstr ""

#. type: section{#2}
#: GBP-Part4.tex:586
msgid "Hochladen nach \\textit{people.debian.org}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:586
msgid ""
"\\nwenddocs{}\\nwbegincode{35}\\sublabel{NW4N9Oy2-1onIoV-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1onIoV-1}}}\\moddef{Upload2PeopleDO~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1onIoV-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-1Zg9xf-1}}\\nwenddeflinemarkup "
"function Upload2PeopleDO \\{ # Called by TaskSelect"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:615
msgid ""
"if [ $Upl -eq 2 ] then # For people.d.o you can not use dput if whiptail "
"--title \"Archive on people.d.o\" \\\\ --yesno \"Does the directory "
"public_html/$\\{OrigName\\} \\\\ already exsist at people.d.o?\\\\n \\\\ If "
"not you have to enter the following commands\\\\n \\\\ in a separate "
"terminal:\\\\n\\\\n \\\\ ssh <user>@people.debian.org\\\\n \\\\ mkdir "
"--parents public_html/$\\{OrigName\\}\" \\\\ --yes-button \"Yes\" "
"--no-button \"No\" 15 60 then UploadFilesSelect "
"\\LA{}Upload2PeopleDO3~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-Wujn0-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-1Zg9xf-1}}\\nwendcode{}\\nwbegindocs{36}\\nwdocspar "
"\\nwenddocs{}\\nwbegincode{37}\\sublabel{NW4N9Oy2-1Zg9xf-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1Zg9xf-1}}}\\moddef{UploadFilesSelect~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1Zg9xf-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-38Lz4Y-1}}\\nwenddeflinemarkup "
"function UploadFilesSelect \\{ # Called by Upload2PeopleDO UploadLocal "
"UplFL=$(cat $\\{UplPaket\\} | grep --after-context=10 'Files: *')  "
"UplFL1=$(echo $\\{UplFL\\} | sed --expression='s/Files: //')  i=1 while [ $i "
"-lt 6 ] do c=$(expr $\\{i\\} '*' '5')  UplFL2=$\\{UplFL2\\}\" \"$(echo "
"$UplFL1 | cut --delimiter=\" \" -f$\\{c\\})  i=$(expr $\\{i\\} '+' '1')  "
"done \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:638
msgid ""
"\\LA{}Upload2PeopleDO~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-1onIoV-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-38Lz4Y-1}}\\nwendcode{}\\nwbegindocs{38}\\nwdocspar "
"\\nwenddocs{}\\nwbegincode{39}\\sublabel{NW4N9Oy2-Wujn0-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-Wujn0-1}}}\\moddef{Upload2PeopleDO3~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-Wujn0-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-1onIoV-1}}\\nwenddeflinemarkup "
"if whiptail --title \"Upload file?\" \\\\ --yesno \"Should the following "
"files\\\\n$\\{UplFL2\\}\\\\n \\\\ have to be uploaded?\" --yes-button "
"\"Yes\" \\\\ --no-button \"No\" 15 60 then if [ -z \"$\\{pdoaccount\\}\" ] "
"then pdoaccount=$(whiptail --title \"Account at people.debian.org\" \\\\ "
"--inputbox \"Please insert the name of your account on\\\\n \\\\ "
"people.debian.org\" \\\\ --cancel-button \"Cancel\" 15 60 3>&2 2>&1 1>&3)  "
"if [ -z \"$\\{pdoaccount\\}\" ] then echo \"Please insert the name of your "
"account on\\\\n \\\\ people.debian.org\" read pdoaccount fi changeflag=1 fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:654
msgid ""
"if ! whiptail --title \"Account name\" \\\\ --yesno \"The name of your "
"account on people.debian.org:\\\\n \\\\ $\\{pdoaccount\\}\" --yes-button "
"\"Yes\" --no-button \"No\" 15 60 then pdoaccount=$(whiptail --title \" "
"Account name\" \\\\ --inputbox \"Name of your account on "
"people.debian.org:\" \\\\ --cancel-button \"Cancel\" 15 60 3>&2 2>&1 1>&3)  "
"if [ -z \"$\\{pdoaccount\\}\" ] then echo \"Please insert the name of your "
"account on\\\\n \\\\ people.debian.org\" read pdoaccount fi changeflag=1 fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:660
msgid ""
"if [ $changeflag -eq 1 ] then echo 'pdoaccount='$\\{pdoaccount\\} >> "
"$\\{ConfigPath\\}$\\{OrigName\\} changeflag=0 fi"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:680
msgid ""
"cd $\\{ProjectPath\\}/$\\{OrigName\\} if scp -p $\\{UplFL2\\} "
"$\\{pdoaccount\\}@people.debian.org:/home/$\\{pdoaccount\\}/public_html/$\\{OrigName\\} "
"then echo \"$\\{UplFL2\\} were uploaded to p.d.o.\" >> $\\{log\\} else echo "
"\"Something went wrong while uploading to p.d.o.\" >> $\\{log\\} echo "
"\"Tried to execute this command:\" >> $\\{log\\} echo \"scp -p "
"\"$\\{UplFL2\\}\" \"$\\{pdoaccount\\}\"@people.debian.org:/home/\"\\\\ "
"$\\{pdoaccount\\}\"/public_html/\"$\\{OrigName\\} >> $\\{log\\} fi "
"pdoarchivetext=\"If the archive on people.d.o should be\\\\n \\\\ used too, "
"you have to enter the following commands:\\\\n \\\\ ssh "
"<user>@people.debian.org\\\\n \\\\ cd public_html/$\\{OrigName\\}\\\\n \\\\ "
"apt-ftparchive packages . > Packages\\\\n \\\\ apt-ftparchive sources . > "
"Sources\\\\n \\\\ cat Packages | gzip -c > Packages.gz\\\\n \\\\ cat Sources "
"| gzip -c > Sources.gz\\\\n \\\\ apt-ftparchive release . > Release\""
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:694
msgid ""
"if whiptail --title \"Archive on people.d.o\" \\\\ --yesno "
"\"$\\{pdoarchivetext\\}\\\\n\\\\n \\\\ Do you like to copy and paste these "
"commands?\" \\\\ --yes-button \"Yes\" --no-button \"No\" 15 60 then echo -e "
"$pdoarchivetext echo -e \"\\\\nPlease press any key to continue!\" read x fi "
"fi fi fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:697
msgid ""
"\\LA{}UpdateLocalRepo~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-BfToG-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-1onIoV-1}}\\nwendcode{}\\nwbegindocs{40}\\nwdocspar"
msgstr ""

#. type: section{#2}
#: GBP-Part4.tex:699
msgid "Lokales Repositorium"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:702
msgid ""
"Es kommt immer wieder vor, dass Pakete gebaut werden müssen, die für das "
"eigentliche Projekt als Abhängigkeiten benötigt werden.  Um die Zeit für den "
"Gang durch die New-Queue zu überbrücken, können diese auch lokal für das "
"Bauen in der \\textit{chroot} bereitgestellt werden."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:720
msgid ""
"\\nwenddocs{}\\nwbegincode{41}\\sublabel{NW4N9Oy2-3e9Pf3-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3e9Pf3-1}}}\\moddef{UploadLocal~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3e9Pf3-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-BfToG-1}}\\nwenddeflinemarkup "
"function UploadLocal \\{ # Called by TaskSelect if [ $Upl -eq 4 ] then # "
"Provide for local chroot UploadFilesSelect if whiptail --title \" Files "
"Uploaded?\" \\\\ --yesno \"Should the following files\\\\n \\\\ "
"$\\{UplFL2\\}\\\\nhave to be uploaded?\" 15 60 then cd "
"$\\{ProjectPath\\}/$\\{OrigName\\} sudo cp $\\{UplFL2\\} "
"/var/local/repository UpdateLocalRepo fi fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:723
msgid ""
"\\LA{}ChangeEntry~{\\nwtagstyle{}\\subpageref{nw@notdef}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-BfToG-1}}\\nwendcode{}\\nwbegindocs{42}\\nwdocspar"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:725
msgid ""
"Die folgende Funktion gibt es bereits als Shellscript unter "
"\\textit{/usr/local/bin/LocalNewRepo}."
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:730
msgid ""
"\\nwenddocs{}\\nwbegincode{43}\\sublabel{NW4N9Oy2-BfToG-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9Oy2-BfToG-1}}}\\moddef{UpdateLocalRepo~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-BfToG-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9Oy2-Wujn0-1}}\\nwenddeflinemarkup "
"function UpdateLocalRepo \\{ # Called by UploadLocal cd "
"/var/local/repository"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:738
msgid ""
"# Make package archives writable # (not only for root)  sudo chmod o+w "
"Packages sudo chmod o+w Sources sudo chmod o+w Packages.gz sudo chmod o+w "
"Sources.gz sudo chmod o+w Release"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:745
msgid ""
"# Use apt-ftparchive to update package archives sudo apt-ftparchive packages "
". > Packages && sudo apt-ftparchive sources . > Sources && sudo cat Packages "
"| gzip -c > Packages.gz && sudo cat Sources | gzip -c > Sources.gz && sudo "
"apt-ftparchive release . > Release"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:753
msgid ""
"# Reset rights sudo chmod o-w Packages sudo chmod o-w Sources sudo chmod o-w "
"Packages.gz sudo chmod o-w Sources.gz sudo chmod o-w Release \\}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:756
msgid ""
"\\LA{}UploadLocal~{\\nwtagstyle{}\\subpageref{NW4N9Oy2-3e9Pf3-1}}\\RA{} "
"\\nwused{\\\\{NW4N9Oy2-Wujn0-1}}\\nwendcode{}"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:783
msgid ""
"\\nwixlogsorted{c}{{ChangeEntry}{nw@notdef}{\\nwixu{NW4N9Oy2-3e9Pf3-1}}}\\nwixlogsorted{c}{{CreateSignature}{NW4N9Oy2-fT69x-1}{\\nwixd{NW4N9Oy2-fT69x-1}\\nwixu{NW4N9Oy2-3stkYg-1}}}\\nwixlogsorted{c}{{Dput2FtpMaster}{NW4N9Oy2-2eHzlG-1}{\\nwixd{NW4N9Oy2-2eHzlG-1}\\nwixu{NW4N9Oy2-3qFEIZ-1}}}\\nwixlogsorted{c}{{GettingFingerprint}{NW4N9Oy2-3stkYg-1}{\\nwixu{NW4N9Oy2-4SzZ52-1}\\nwixd{NW4N9Oy2-3stkYg-1}}}\\nwixlogsorted{c}{{PrepareUploading}{NW4N9Oy2-2h4AeG-1}{\\nwixd{NW4N9Oy2-2h4AeG-1}}}\\nwixlogsorted{c}{{PrepareUploading3}{NW4N9Oy2-2fQbcs-1}{\\nwixu{NW4N9Oy2-2h4AeG-1}\\nwixd{NW4N9Oy2-2fQbcs-1}}}\\nwixlogsorted{c}{{PrepareUploading5}{NW4N9Oy2-3vUIOc-1}{\\nwixu{NW4N9Oy2-2fQbcs-1}\\nwixd{NW4N9Oy2-3vUIOc-1}}}\\nwixlogsorted{c}{{SelectUploadTarget}{NW4N9Oy2-44n7uC-1}{\\nwixu{NW4N9Oy2-3vUIOc-1}\\nwixd{NW4N9Oy2-44n7uC-1}}}\\nwixlogsorted{c}{{SelectUploadTarget1}{NW4N9Oy2-4XRSRb-1}{\\nwixu{NW4N9Oy2-44n7uC-1}\\nwixd{NW4N9Oy2-4XRSRb-1}}}\\nwixlogsorted{c}{{StartTasks}{nw@notdef}{\\nwixu{NW4N9Oy2-2fcvCo-1}}}\\nwixlogsorted{c}{{TaskSelect10}{NW4N9Oy2-1qX7nC-1}{\\nwixu{NW4N9Oy2-47oStO-1}\\nwixd{NW4N9Oy2-1qX7nC-1}}}\\nwixlogsorted{c}{{TaskSelect11}{NW4N9Oy2-2fcvCo-1}{\\nwixu{NW4N9Oy2-1qX7nC-1}\\nwixd{NW4N9Oy2-2fcvCo-1}}}\\nwixlogsorted{c}{{TaskSelect9}{NW4N9Oy2-47oStO-1}{\\nwixd{NW4N9Oy2-47oStO-1}}}\\nwixlogsorted{c}{{UpdateLocalRepo}{NW4N9Oy2-BfToG-1}{\\nwixu{NW4N9Oy2-Wujn0-1}\\nwixd{NW4N9Oy2-BfToG-1}}}\\nwixlogsorted{c}{{Upload2FtpMaster}{NW4N9Oy2-2XJq8-1}{\\nwixd{NW4N9Oy2-2XJq8-1}\\nwixu{NW4N9Oy2-2eHzlG-1}}}\\nwixlogsorted{c}{{Upload2Mentors}{NW4N9Oy2-3qFEIZ-1}{\\nwixu{NW4N9Oy2-fT69x-1}\\nwixd{NW4N9Oy2-3qFEIZ-1}}}\\nwixlogsorted{c}{{Upload2OwnServer}{NW4N9Oy2-1xqyce-1}{\\nwixd{NW4N9Oy2-1xqyce-1}\\nwixu{NW4N9Oy2-4XRSRb-1}}}\\nwixlogsorted{c}{{Upload2PeopleDO}{NW4N9Oy2-1onIoV-1}{\\nwixd{NW4N9Oy2-1onIoV-1}\\nwixu{NW4N9Oy2-1Zg9xf-1}}}\\nwixlogsorted{c}{{Upload2PeopleDO3}{NW4N9Oy2-Wujn0-1}{\\nwixu{NW4N9Oy2-1onIoV-1}\\nwixd{NW4N9Oy2-Wujn0-1}}}\\nwixlogsorted{c}{{Upload2Salsa}{NW4N9Oy2-3M9mGy-1}{\\nwixd{NW4N9Oy2-3M9mGy-1}\\nwixu{NW4N9Oy2-1xqyce-1}}}\\nwixlogsorted{c}{{Upload2Salsa5}{NW4N9Oy2-4SzZ52-1}{\\nwixu{NW4N9Oy2-3M9mGy-1}\\nwixd{NW4N9Oy2-4SzZ52-1}}}\\nwixlogsorted{c}{{UploadFilesSelect}{NW4N9Oy2-1Zg9xf-1}{\\nwixu{NW4N9Oy2-38Lz4Y-1}\\nwixd{NW4N9Oy2-1Zg9xf-1}}}\\nwixlogsorted{c}{{UploadLocal}{NW4N9Oy2-3e9Pf3-1}{\\nwixd{NW4N9Oy2-3e9Pf3-1}\\nwixu{NW4N9Oy2-BfToG-1}}}\\nwixlogsorted{c}{{UploadUsingDput}{NW4N9Oy2-38Lz4Y-1}{\\nwixd{NW4N9Oy2-38Lz4Y-1}\\nwixu{NW4N9Oy2-2XJq8-1}}}\\nwbegindocs{44}\\nwdocspar "
"In die \\textit{Chroot-Umgebung}, hier "
"\\textit{/var/cache/pbuilder/base.cow}, wird in die Datei "
"\\textit{/etc/apt/sources.list} folgendes eingefügt:"
msgstr ""

#. type: verbatim
#: GBP-Part4.tex:787
#, no-wrap
msgid ""
"deb [trusted=yes] file:///var/local/repository ./\n"
"deb-src [trusted=yes] file:///var/local/repository ./"
msgstr ""

#. type: Plain text
#: GBP-Part4.tex:789
msgid "\\nwenddocs{}"
msgstr ""
