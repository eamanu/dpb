# French translations for BuildWithGBP Rolling package
# Copyright (C) 2021 Mechtilde & Michael Stehmann
# This file is distributed under the same license as the BuildWithGBP package.
# Automatically generated, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: BuildWithGBP Rolling Release\n"
"POT-Creation-Date: 2021-03-30 17:40+0200\n"
"PO-Revision-Date: 2021-03-30 17:40+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#.  !TEX root = BuildWithGBP.nw% ===> this file was generated automatically by noweave --- better not edit it
#. type: part{#2}
#: GBP-Part5.tex:3
msgid "Weitere Bestandteile des Skriptes"
msgstr ""

#. type: chapter{#2}
#: GBP-Part5.tex:5
msgid "Weitere Aufgaben"
msgstr ""

#. type: section{#2}
#: GBP-Part5.tex:11
msgid "Neuen Branch erstellen"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:11
msgid ""
"\\nwfilename{Part5."
"nw}\\nwbegincode{1}\\sublabel{NW4N9OxL-1We84c-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9OxL-1We84c-1}}}\\moddef{CreateNewBranch~{\\nwtagstyle{}\\subpageref{NW4N9OxL-1We84c-1}}}\\endmoddef"
"\\nwstartdeflinemarkup\\nwenddeflinemarkup function CreateNewBranch \\{ # "
"Called by TaskSelect"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:23
msgid ""
"# Creates a new branch (for backports or proposed-updates)  DebianBranches "
"whiptail --title \"Recent branches\" \\\\ --msgbox \"Recent branches:\\\\n$"
"\\{bl\\}\" 15 60 bName=\"\" bName=$(whiptail --inputbox \"Name of the new "
"branch:\" \\\\ --cancel-button \"Cancel\" 15 60 3>&2 2>&1 1>&3)  if [ $"
"\\{bName\\} != \"\" ] then ## Create new branch in git git checkout -b $"
"\\{bName\\}"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:26
msgid "## Change config file - make new branch to recent one ChangeEntry"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:33
msgid ""
"whiptail --title \"New branch was created\" \\\\ --msgbox \"New branch $"
"\\{bName\\} was created\" 15 60 echo \"New branch $\\{bName\\} was created\" "
">> $\\{log\\} Distro4Branch fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:36
msgid ""
"\\LA{}ParseConfig~{\\nwtagstyle{}\\subpageref{nw@notdef}}\\RA{} "
"\\nwnotused{CreateNewBranch}\\nwendcode{}\\nwbegindocs{2}\\nwdocspar"
msgstr ""

#. type: section{#2}
#: GBP-Part5.tex:39
msgid "Eingabe des Namens oder der IP eines eigenen Git-Servers"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:45
msgid ""
"Mit dieser Funktion, die von der Aufgabenauswahl (Kapitel~\\ref{sec:"
"Aufgabenauswahl}, Seite~\\pageref{sec:Aufgabenauswahl}) aufgerufen werden "
"kann, werden der Name oder die IP eines eigenen Git-Servers in die "
"Konfigurationsdatei eingetragen. Die Eingabe eines Namens setzt eine "
"funktionierende Namensauflösung voraus."
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:49
msgid ""
"\\nwenddocs{}\\nwbegincode{3}\\sublabel{NW4N9OxL-"
"pLB35-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9OxL-"
"pLB35-1}}}\\moddef{OwnServer~{\\nwtagstyle{}\\subpageref{NW4N9OxL-"
"pLB35-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\\\{NW4N9OxL-"
"prxKJ-1}}\\nwenddeflinemarkup function OwnServer \\{ # Called by TaskSelect"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:72
msgid ""
"# Set name or IP of own git server ServerName=$(whiptail --inputbox \"Name "
"or IP-address of your git server:\" \\\\ --cancel-button \"Cancel\" 15 60 "
"3>&2 2>&1 1>&3)  if [ -z \"$\\{ServerName\\}\" ] then echo \"Name or IP of "
"your git server:\" read ServerName fi if [ -n \"$ServerName\" ] then sn="
"$(grep --count 'ServerName=' $\\{ConfigPath\\}$\\{OrigName\\})  if [ $sn -ge "
"1 ] then # ReplaceConfigLines needs two parameters: # name of the variable "
"and new value ReplaceConfigLines 'ServerName' $\\{ServerName\\} else echo "
"'ServerName='$\\{ServerName\\} >> $\\{ConfigPath\\}$\\{OrigName\\} fi "
"AddGitServer fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:88
msgid ""
"\\LA{}TaskSelect~{\\nwtagstyle{}\\subpageref{nw@notdef}}\\RA{} \\nwused{\\"
"\\{NW4N9OxL-prxKJ-1}}\\nwendcode{}\\nwbegindocs{4}\\nwdocspar "
"\\section{Prov. AddGitServer} "
"\\nwenddocs{}\\nwbegincode{5}\\sublabel{NW4N9OxL-"
"prxKJ-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9OxL-"
"prxKJ-1}}}\\moddef{AddGitServer~{\\nwtagstyle{}\\subpageref{NW4N9OxL-"
"prxKJ-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwenddeflinemarkup function "
"AddGitServer \\{ # Called by OwnServer serverlist=$(git remote -v)  if "
"whiptail --title \"Recent remote servers\" \\\\ --yesno \"$\\{serverlist\\}\\"
"\\nAdd git remote server 'home'?\" \\\\ --yes-button \"Yes\" \\\\ --no-"
"button \"No\" 15 60 then AddHomeServer fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:91
msgid ""
"\\LA{}OwnServer~{\\nwtagstyle{}\\subpageref{NW4N9OxL-pLB35-1}}\\RA{} "
"\\nwnotused{AddGitServer}\\nwendcode{}\\nwbegindocs{6}\\nwdocspar"
msgstr ""

#. type: chapter{#2}
#: GBP-Part5.tex:93
msgid "Kopf des Skriptes"
msgstr ""

#. type: section{#2}
#: GBP-Part5.tex:96
msgid "Shebang"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:96
msgid ""
"Am Anfang des Skriptes befinden sich die \\textit{Shebang}, Vermerke über "
"die Autoren, die Version und die Lizenz."
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:99
msgid ""
"\\nwenddocs{}\\nwbegincode{7}\\sublabel{NW4N9OxL-42a6Y2-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9OxL-42a6Y2-1}}}\\moddef{build-"
"gbp.sh~{\\nwtagstyle{}\\subpageref{NW4N9OxL-42a6Y2-1}}}\\endmoddef"
"\\nwstartdeflinemarkup\\nwenddeflinemarkup #!/bin/bash"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:103
msgid ""
"\\LA{}copyright~{\\nwtagstyle{}\\subpageref{NW4N9OxL-3CaW5t-1}}\\RA{} "
"\\nwnotused{build-gbp.sh}\\nwendcode{}\\nwbegindocs{8}\\nwdocspar"
msgstr ""

#. type: section{#2}
#: GBP-Part5.tex:103
msgid "Copyright-Vermerk"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:108
msgid ""
"Auf die \\textit{Shebang} folgt der Copyright-Vermerk.  "
"\\nwenddocs{}\\nwbegincode{9}\\sublabel{NW4N9OxL-3CaW5t-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9OxL-3CaW5t-1}}}\\moddef{copyright~{\\nwtagstyle{}\\subpageref{NW4N9OxL-3CaW5t-1}}}\\endmoddef"
"\\nwstartdeflinemarkup\\nwusesondefline{\\"
"\\{NW4N9OxL-42a6Y2-1}}\\nwenddeflinemarkup # Copyright 2019 Mechtilde and "
"Michael Stehmann <mechtilde@debian.org> # version 0.8.1"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:113
msgid ""
"# This program is free software; you can redistribute it and/or modify # it "
"under the terms of the GNU General Public License as published by # the Free "
"Software Foundation; either version 3 of the License, or # (at your option) "
"any later version."
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:118
msgid ""
"# This program is distributed in the hope that it will be useful, # but "
"WITHOUT ANY WARRANTY; without even the implied warranty of # MERCHANTABILITY "
"or FITNESS FOR A PARTICULAR PURPOSE.  See the # GNU General Public License "
"for more details."
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:123
msgid ""
"# You should have received a copy of the GNU General Public License # along "
"with this program; if not, write to the Free Software # Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, # MA 02110-1301, USA."
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:129
msgid ""
"# Dependencies: git-buildpackage, build-essential, less, pbuilder, # "
"pristine-tar, sudo, unzip, cowbuilder, cowdancer, debmake, quilt, # locate, "
"jq, lintian, devscripts # gradle-debian-helper, maven-debian-helper, "
"libmaven-bundle-plugin-java, # mozilla-devscripts"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:134
msgid ""
"\\LA{}Header~{\\nwtagstyle{}\\subpageref{NW4N9OxL-2Cop3h-1}}\\RA{} "
"\\nwused{\\\\{NW4N9OxL-42a6Y2-1}}\\nwendcode{}\\nwbegindocs{10}\\nwdocspar "
"Sodann werden die Abhängigkeiten aufgeführt (Kapitel~\\ref{sec:"
"ScriptDependencies}, Seite~\\pageref{sec:ScriptDependencies})."
msgstr ""

#. type: section{#2}
#: GBP-Part5.tex:138
msgid "Funktionsheader"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:138
msgid ""
"\\nwenddocs{}\\nwbegincode{11}\\sublabel{NW4N9OxL-2Cop3h-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9OxL-2Cop3h-1}}}\\moddef{Header~{\\nwtagstyle{}\\subpageref{NW4N9OxL-2Cop3h-1}}}\\endmoddef"
"\\nwstartdeflinemarkup\\nwusesondefline{\\"
"\\{NW4N9OxL-3CaW5t-1}}\\nwenddeflinemarkup ##########################"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:140
msgid "# Definitions of functions"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:142
msgid "##########################"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:145
msgid ""
"\\LA{}DebugRP~{\\nwtagstyle{}\\subpageref{NW4N9OxL-R1FC0-1}}\\RA{} "
"\\nwused{\\\\{NW4N9OxL-3CaW5t-1}}\\nwendcode{}\\nwbegindocs{12}\\nwdocspar"
msgstr ""

#. type: section{#2}
#: GBP-Part5.tex:147
msgid "Funktion zur Fehlersuche"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:151
msgid ""
"Die folgende Funktion zeigt einen Pfad und ermöglicht gegebenenfalls einen "
"Ausstieg aus dem Programm.  Sie dient dem Debuging und ist normalerweie "
"ungenutzt"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:165
msgid ""
"\\nwenddocs{}\\nwbegincode{13}\\sublabel{NW4N9OxL-"
"R1FC0-1}\\nwmargintag{{\\nwtagstyle{}\\subpageref{NW4N9OxL-"
"R1FC0-1}}}\\moddef{DebugRP~{\\nwtagstyle{}\\subpageref{NW4N9OxL-"
"R1FC0-1}}}\\endmoddef\\nwstartdeflinemarkup\\nwusesondefline{\\"
"\\{NW4N9OxL-2Cop3h-1}}\\nwenddeflinemarkup function DebugRP \\{ # Function "
"to show a path and give an opportunity to exit # It is for debugging descstr="
"$\\{1\\} pathstr=$\\{2\\} if ! whiptail --title \"Shows the path\" \\\\ --"
"yesno \"$\\{descstr\\}= $\\{pathstr\\}?\" --yes-button \"Yes\" \\\\ --no-"
"button \"No\" 15 60 then exit fi \\}"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:168
msgid ""
"\\LA{}ReplaceConfigLines~{\\nwtagstyle{}\\subpageref{nw@notdef}}\\RA{} "
"\\nwused{\\\\{NW4N9OxL-2Cop3h-1}}\\nwendcode{}"
msgstr ""

#. type: Plain text
#: GBP-Part5.tex:180
msgid ""
"\\nwixlogsorted{c}{{AddGitServer}{NW4N9OxL-prxKJ-1}{\\nwixd{NW4N9OxL-"
"prxKJ-1}}}\\nwixlogsorted{c}{{build-gbp.sh}{NW4N9OxL-42a6Y2-1}"
"{\\nwixd{NW4N9OxL-42a6Y2-1}}}\\nwixlogsorted{c}{{copyright}"
"{NW4N9OxL-3CaW5t-1}"
"{\\nwixu{NW4N9OxL-42a6Y2-1}\\nwixd{NW4N9OxL-3CaW5t-1}}}\\nwixlogsorted{c}"
"{{CreateNewBranch}{NW4N9OxL-1We84c-1}"
"{\\nwixd{NW4N9OxL-1We84c-1}}}\\nwixlogsorted{c}{{DebugRP}{NW4N9OxL-R1FC0-1}"
"{\\nwixu{NW4N9OxL-2Cop3h-1}\\nwixd{NW4N9OxL-R1FC0-1}}}\\nwixlogsorted{c}"
"{{Header}{NW4N9OxL-2Cop3h-1}"
"{\\nwixu{NW4N9OxL-3CaW5t-1}\\nwixd{NW4N9OxL-2Cop3h-1}}}\\nwixlogsorted{c}"
"{{OwnServer}{NW4N9OxL-pLB35-1}{\\nwixd{NW4N9OxL-pLB35-1}\\nwixu{NW4N9OxL-"
"prxKJ-1}}}\\nwixlogsorted{c}{{ParseConfig}{nw@notdef}"
"{\\nwixu{NW4N9OxL-1We84c-1}}}\\nwixlogsorted{c}{{ReplaceConfigLines}"
"{nw@notdef}{\\nwixu{NW4N9OxL-R1FC0-1}}}\\nwixlogsorted{c}{{TaskSelect}"
"{nw@notdef}{\\nwixu{NW4N9OxL-pLB35-1}}}\\nwbegindocs{14}\\nwdocspar "
"\\nwenddocs{}"
msgstr ""
