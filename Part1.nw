% !TEX root = BuildWithGBP.nw.tex
\part{Überblick}
\chapter{Lizenz}

Der Text des Buches \glqq{}\texttt{Debian}-Pakete bauen mit 
\textit{Git-Buildpackage}\grqq{} von 
Mechtilde und Michael Stehmann steht unter der \index{Lizenz} Lizenz 
\textbf{Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 
4.0 International Lizenz \index{Creative Commons}
(CC BY-SA 4.0)}\cite{CreativeCommon40-2013}.

Das beschriebene Skript steht unter der \textbf{GNU General Public License 
Version 3} \index{GNU General Public License} oder nach Ihrer Wahl einer 
späteren Version\cite{GPLv3}.

Copyright: \textsuperscript{\textcopyright} 2012-2021 Mechtilde Stehmann, 
Michael Stehmann

\chapter{Wer sollte dieses Buch lesen?}

Die Ausführungen in diesem Buch sind besonders für die Nutzer des Skriptes 
interessant. 
Aber auch Menschen, die sich allgemein für das Paketieren \index{Paketieren}
für eine Distribution 
interessieren, werden in diesem Buch Informationen finden können.

Dieses Buch will kein \glqq{}Lehrbuch\grqq{} des \texttt{Debian}-Paketbaus sein. 
Es ist eher ein Erfahrungsbericht, wobei die Erfahrungen \glqq{}in Code 
gegossen\grqq{} worden sind.

Das Buch beschreibt, wie \texttt{Debian}-Pakete auf der Basis eines Git-Repositoriums 
mit den Programmen aus dem Paket \textit{git-buildpackage} \cite{Guenther2006-2017} 
und anderen 
nützlichen Befehlen erstellt werden. Am Ende sollte der Leser in der Lage 
sein, mit der Hilfe des dargestellten Programms und der Beschreibungen der 
einzelnen Schritte \glqq{}veröffentlichungsreife\grqq{} 
\texttt{Debian}-Pakete zu bauen.

Dieses Buch kann auch dazu dienen, Probleme zu verstehen, die beim 
Paketieren von \texttt{Debian}-Paketen auftreten können.

Paketieren ist im Grunde nicht schwierig. 
Es gibt aber immer wieder neue Herausforderungen. 
Paketieren macht daher Spaß.

\chapter{Wie entsteht dieses Buch?}
\section{Motivation}

Was treibt uns, ein solches Buch zu schreiben?

Dazu muss man folgendes wissen:

Beim Paketieren werden viele Befehle in einer sinnvollen Reihenfolge an 
der Shell ausgeführt. Außerdem müssen viele kleine Dateien gepflegt und 
eingebunden werden. Kleinste Fehler und Ungenauigkeiten führen meist dazu, dass 
das Paket nicht korrekt gebaut werden kann. Auch ist es aufwändig und 
fehlerträchtig, immer wieder die korrekten Optionen einzusetzen. 

Um diese Fehlerquellen möglichst klein zu halten, wurden diese Schritte in einem 
Shell-Skript zusammengefasst. Im Laufe der Zeit und mit jedem weiteren Paket 
wächst nun dieses Skript und wird auch immer weiter verfeinert. Daraus ist 
bisher schon ein umfangreiches Programm-Skript geworden.

Als Mechtilde anfing, sich mit dem Bauen von \texttt{Debian}-Paketen zu 
beschäftigen, stand die Frage im Raum, wie sich diese vielen Schritte 
dokumentieren und automatisieren lassen. Zur Automatisierung ist dann dieses 
Shell-Skript entstanden Der Bedarf der Dokumentation konnte von Anfang an nicht 
mit Kommentaren, weder in den einzelnen Dateien noch in diesem Programm-Skript 
gedeckt werden.

Deshalb haben wir auch schon früh angefangen, unsere Schritte beim Paketieren 
ausführlich festzuhalten. Ein besonderes Augenmerk haben wir auf
Beschreibungen gelegt, welche getroffene Entscheidungen nachvollziehbar und 
überprüfbar machen. Dies erleichtert notwendige Veränderungen.

Daher haben wir auch soweit wie möglich bei der Angaben von Optionen die 
Langform gewählt. Dies erleichtert die Lesbarkeit.

Die Dokumentation soll also sowohl das eigentliche Paketieren beschreiben, 
als auch das Skript erläutern.

Die \texttt{Debian}-Distribution \index{Distribution} ist das Werk vieler 
Menschen. Sie besteht aus mehreren zehntausenden Paketen. 
Das Bauen der Pakete ist eine wesentliche Aufgabe der Paket-Betreuer. 
Viele Paketbetreuer nutzen hierzu eigene Skripte. 
Die Veröffentlichung eines solchen Skriptes ist daher ein Wagnis. 
Wenn unser Skript einigen Paketbetreuern das Leben erleichtert und Neulinge 
an das Paketbauen heranführt, hat sich dieses Wagnis gelohnt.

Das beschriebene Skript bezieht sich nicht auf ein bestimmtes 
\texttt{Debian}-Paket. 
Vielmehr sollen damit generell einfache \texttt{Debian}-Pakete gebaut 
werden können.

Es werden die Schritte beschrieben, die wir für das Paketieren der von 
Mechtilde betreuten Pakete benötigen. Das Skript erhebt nicht den Anspruch, 
damit könne man aus jedem Quellcode ein Debian-Paket bauen.

An vielen Stellen kann und muss man auch manuell eingreifen. 
Hierzu soll die Beschreibung der Vorgänge beim Paketieren eine Hilfe sein. 

Dass das Programmskript die Möglichkeit manueller Eingriffe voraussetzt, 
macht es erforderlich, dass das Programmskript immer wieder prüft, ob 
die Voraussetzungen, von denen die Verfasser ausgegangen sind, auch 
tatsächlich vorliegen. Diese Notwendigkeit erhöht leider den Umfang und 
die Komplexität des Skriptes und damit auch den Umfang des Buches.

\section{Baustellen}

Das Buch und das Skript sind immer noch \glqq{}Baustellen\grqq{}, weil 
immer wieder neue Erfahrungen einfließen.

Das Buch ist in deutscher, die Oberfläche des Skripts in englischer 
Sprache verfasst. 
Lokalisierungen sind willkommen. Für eine englischsprachige Übersetzung 
wurden die benötigten \textit{*.po}-Dateien bereits erstellt.

Die Veröffentlichung \index{Veröffentlichung} des Quelltextes \index{Quelltext} 
erfolgt im vom \texttt{Debian}-Projekt bereitgestellten 
\texttt{Git}-Repositorium\footnote{\url{https://salsa.debian.org/ddp-team/dpb}}. 
Auch die fertigen Dokumente können als \textit{*.pdf} und \textit{*.epub} 
heruntergeladen werden\footnote{\url{https://people.debian.org/~mechtilde/Dokumentation/}}.

Irgendwann kann dieser Abschnitt hoffentlich entfallen.

\section{Werkzeuge}
\glqq{}Schöner Leben mit Dokumentation\grqq{} ist ein geflügeltes Wort 
in unserer \textit{peer group}.

Mit welchen \index{Werkzeuge} Werkzeugen lässt sich eine solche 
Dokumentation erstellen?
Stehen diese Werkzeuge auch als \texttt{Debian}-Pakete zur Verfügung?

Einen wichtigen Hinweis dazu hat Mechtilde auf einer Veranstaltung zum 
\textit{Software Freedom Day 2012} in Köln erhalten. 
Dort hat sie die Möglichkeiten von \textit{noweb}
\cite{NoWeb2018}\footnote{s.a. \url{https://de.wikipedia.org/wiki/Noweb}} 
\index{noweb} kennengelernt.
Dies bedeutete auch, sich mit \LaTeX{} \index{\LaTeX{}}näher zu beschäftigen.

In dieser Kombination ist es möglich, Code und dessen Beschreibung in 
\emph{einem} Dokument zu pflegen. Donald Knuth bezeichnet dies als \index{Literate Programming}
\glqq{}Literate Programming\grqq{}\footnote{\url{http://www.literateprogramming.com/}}

Weiter haben wir uns damit beschäftigt, dass sich mit \LaTeX{} neben 
PDF-Dokumenten \index{PDF-Dokument} auch Dokumente im \texttt{EPUB}-Format 
\index{EPUB-Dokument} erstellen lassen. Diese können auch einem E-Book-Reader 
\index{E-Book} gelesen werden.(Kapitel~\ref{sec:CreateBook}, 
Seite~\pageref{sec:CreateBook})

Außerdem gibt es mit \textit{po4a} \index{po4a} eine Möglichkeit, in 
mehreren Schritten das Gesamtdokument so in kleine Häppchen zu zerlegen 
(\textit{*.po}-Dateien)\index{PO-Dateien}, dass es kontinuierlich auch in andere 
Sprachen übersetzt und aktualisiert werden kann. 
(Kapitel~\ref{sec:CreateTranslationFiles}, 
Seite~\pageref{sec:CreateTranslationFiles})

Ein solches Dokument haben Sie nun vor sich.

Das Literaturverzeichnis wird mit \textit{jabref} erstellt und gepflegt. 
Die so erstellte Datei kann in das \LaTeX{}-Dokument eingebunden werden.

Als Editor wird \textit{geany} \index{Geany} mit dem Plugin 
\textit{geany-plugin-latex} verwendet.

\textit{Git} \index{git} ist genial. 
Das Bauen erfolgt daher mit den Werkzeugen aus dem Paket \index{git-buildpackage} 
\textit{git-buildpackage}\footnote{\url{https://packages.debian.org/sid/git-buildpackage}}. 

Die Menschen bei \texttt{Debian} haben viele nützliche Programme geschaffen, 
die das Bauen von \texttt{Debian}-Paketen erleichtern und vereinheitlichen. 
Das dargestellte Skript wurde daher \glqq{}auf den Schultern von Riesen\grqq{} geschaffen. 

Es dient dazu, die eingesetzten Hilfsprogramme in zweckmäßiger Reihenfolge 
aufzurufen und mit den notwendigen Optionen zu versehen. 
Es soll seinen Nutzern den Weg weisen, und Ihnen die Arbeit erleichtern .
Um seine Anpassung an die Bedürfnisse seiner Nutzer zu erleichtern, ist 
es ein Shellskript.

\chapter{Konventionen}

Einige Hinweise zum besseren Verständnis des Buches:

\section{System}

Das Buch und besonders das Programm-Skript wurden auf einer \textit{64-Bit-PC}-
Architektur erstellt. Diese wird bei \texttt{Debian} als \textit{amd64} 
bezeichnet. Eine weitere Bezeichnungen für dieses System ist \textit{x86-64}.

\section{Terminologie}

Ein \textbf{neues Paket} ist ein Paket, welches das Programm-Skript noch 
nicht kennt. D.h. zu diesem Paket existiert noch keine Konfigurationsdatei.

Eine \textbf{neue Version} ist eine neue Upstream-Version. Dem Bauen einer 
neuen Version folgt der Bau einer neuen Revision.

Eine \textbf{neue Revision} bezeichnet ein neu hochzuladenes \texttt{Debian}-Paket.

\section{Typographie}
Alle Programmnamen sind in \textit{kursiv} setzt.
Alle Eigennamen sind in \texttt{nicht-proportionaler} Schrift gesetzt.
Hochgestellte Zahlen weisen auf die Fußnoten auf der gleichen Seite.
Zitate auf ein Gesamtdokument weisen in eckigen Klammern [~] direkt auf 
das Literaturverzeichnis.

Alle Optionen der Shell-Kommandos werden in der Langform angegeben, soweit 
dies möglich ist. Dies erhöht die Lesbarkeit.

Für die verwendeten Abkürzungen wird auf die Einträge im Glossar
\footnote{\url{https://wiki.debian.org/Glossary}} verwiesen.

\section{Darstellung des Quellcodes}

Die Darstellung des Quellcodes erfolgt in Teilstücken (sogenannte 
\textit{code chunks}). Die Reihenfolge dieser Code-Teile im Buch entspricht 
oft nicht der Reihenfolge in den Skripten.


